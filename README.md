# Extract ROMs from Gerät 32620 (Sprach-Morse-Generator) to WAV files

Clone and run `make`

## Extracted WAV files

Extracted WAV files can be downloaded [here](https://gitlab.com/stargo/32620_extract/builds/artifacts/master/download?job=wav)

## Ressources

Rom files from: [Crypto Museum](https://www.cryptomuseum.com/spy/owvl/32620/index.htm)

Decoding information from: [Taking a look at Gerät 32620](https://blog.ardy.io/2020/8/geraet-32620/)
