CFLAGS=-Wall

all: convert

2620_8401.bin: $(sort $(wildcard ROMs/2620_8401-*.bin))
	cat $^ >$@

2620_DE_new.bin: $(sort $(wildcard ROMs/2620_DE_new-*.bin))
	cat $^ >$@

2620_span.bin: $(sort $(wildcard ROMs/2620_span-*.bin))
	cat $^ >$@

2620_poland.bin: $(sort $(wildcard ROMs/2620_poland-*.bin))
	cat $^ >$@

german: 2620_8401.bin extract
	./extract 2620_8401.bin german

german-new: 2620_DE_new.bin extract
	./extract 2620_DE_new.bin german-new

spanish: 2620_span.bin extract
	./extract 2620_span.bin spanish

en_poland: 2620_poland.bin extract
	./extract 2620_poland.bin en_poland

convert: german german-new spanish en_poland
	$(MAKE) wav

wav: $(patsubst %.raw,%.wav,$(wildcard *.raw))

%.wav: %.raw
	sox -t raw -r 8000 -b 8 -e unsigned-integer -c 1 $< $@

clean:
	-rm -f extract {german,spanish}-*.raw  {german,spanish}-*.wav 2620_8401.bin 2620_span.bin

.PHONY: clean all convert wav
.SUFFIXES: .wav .raw
